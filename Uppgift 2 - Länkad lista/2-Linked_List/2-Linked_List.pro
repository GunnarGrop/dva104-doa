TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    list.c \
    test_list.c

HEADERS += \
    list.h

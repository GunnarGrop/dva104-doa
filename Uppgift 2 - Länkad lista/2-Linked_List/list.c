#include "list.h"
#include <stdlib.h>
#define NDEBUG
#include <assert.h>

/*Det r helt tilltet att lgga till egna hjlpfunktioner men de befintliga funktionerna fr inte ndras*/

/*Hjalpfunktion till add
  Allokerar minne for en ny nod
  om allokeringen lyckades initieras data samt pekare (pekare initieras till NULL).
  Den nya noden (eller NULL) returneras.*/
static struct node* createListNode(const Data data)
{
    //Glom inte att testa sa att allokeringen lyckades innan du initierar noden
    List newNode = (List) calloc(1, sizeof (List));

    if(newNode != NULL){
        newNode->data = data;
        return newNode;
    }else{
        printf("ERROR: Allocation of memory failed.");
        return NULL;
    }
}

/*Returnera en tom lista - funktionen ar fardig*/
List createEmptyList(void)
{
    return NULL;
}

/*Ar listan tom?
  Returnerar 1 om den r tom, annars 0*/
int isEmpty(const List list)
{
    if(list == NULL){
        return 1;
    }
    return 0; //ersatt med ratt returvarde
}

/*Lagg till nod forst i listan*/
/*Postcondition: Det nya datat ligger forst i listan (testa med assert)*/
void addFirst(List *list, const Data data)
{
    //Anropa createListNode for att skapa den nya noden
    //Glom inte att testa att den nya noden faktiskt kunde skapas/tilldelas minne innan du fortsatter
    //Tank pa att listan kan vara tom nar en ny nod laggs till
    List newNode = createListNode(data);

    if(newNode != NULL){
        if(isEmpty(*list)){
            *list = newNode;
        }else{
            newNode->next = *list;
            newNode->next->previous = newNode;
            *list = newNode;
        }
    }
}

/*Lagg till nod sist i listan
  Tips, nar du hittat ratt plats kan du anvanda funktionen addFirst for att lagga till*/
void addLast(List *list, const Data data)
{
    List lastNode = *list;

    if(!isEmpty(*list)){
        while (lastNode->next != NULL) {
            lastNode = lastNode->next;
        }
    }

    List newNode = createListNode(data);

    if(newNode != NULL){
        if(isEmpty(*list)){
            *list = newNode;
        }else{
            newNode->previous = lastNode;
            lastNode->next = newNode;
        }
    }
}

/*Ta bort forsta noden i listan
  Precondition: listan ar inte tom (testa med assert)
  Noden ska lankas ur och minnet frigoras, resten av listan ska finnas kvar*/
void removeFirst(List *list)
{
    //Glom inte att frigora minnet for den nod som lankas ur listan.
    //Tank pa att listans huvud efter bortlankningen maste peka pa den nod som nu ar forst.
    assert(!isEmpty(*list));

    if(!isEmpty(*list)){
        List nodeToRemove = *list;

        if(nodeToRemove->next != NULL){
            nodeToRemove->next->previous = NULL;
            *list = nodeToRemove->next;
        }else{
            *list = NULL;
        }

        free(nodeToRemove);
    }else{
        printf("Can not remove from empty list.\n");
    }

}

/*Ta bort sista noden i listan
  Precondition: listan ar inte tom (testa med assert)t*/
void removeLast(List *list)
{
    //Glom inte att frigora minnet for den nod som lankas ur listan.
    //Tank pa att den nod som nu ar sist inte pekar nagonstans, dess pekare maste nollstallas
    assert(!isEmpty(*list));

    if(!isEmpty(*list)){
        List nodeToRemove = *list;
        while (nodeToRemove->next != NULL) {
            nodeToRemove = nodeToRemove->next;
        }

        if(nodeToRemove->previous != NULL){
            nodeToRemove->previous->next = NULL;
            nodeToRemove->previous = NULL;
        }else{
            *list = NULL;
        }

        free(nodeToRemove);
    }else{
        printf("Can not remove from empty list.\n");
    }

}

/*Ta bort data ur listan (forsta forekomsten)
  Returnera 1 om datat finns, annars 0
  Tips, nar du hittar ratt nod kan du anvanda en av de ovanstaende funktionerna for att ta bort noden*/
int removeElement(List *list, const Data data)
{
    if(isEmpty(*list)){
        return 0;
    }

    List nodeToRemove = *list;
    while (nodeToRemove->next != NULL && nodeToRemove->data != data) {
        nodeToRemove = nodeToRemove->next;
    }

    if(nodeToRemove->data != data){
        return 0;
    }

    if(nodeToRemove->next != NULL){
        nodeToRemove->next->previous = nodeToRemove->previous;
    }
    if(nodeToRemove->previous != NULL){
        nodeToRemove->previous->next = nodeToRemove->next;
    }

    if(nodeToRemove == *list){
        *list = nodeToRemove->next;
    }

    nodeToRemove->next = NULL;
    nodeToRemove->previous = NULL;
    free(nodeToRemove);

    return 1; //Ersatt med ratt returvarde
}

/*Finns data i listan?
  Om datat finns returneras 1, annars 0
  Tank pa att listan kan vara tom*/
int search(const List list, const Data data)
{
    if(isEmpty(list)){
        return 0;
    }

    List nodeToSearch = list;

    while (nodeToSearch->next != NULL && nodeToSearch->data != data) {
        nodeToSearch = nodeToSearch->next;
    }

    if(nodeToSearch->data != data){
        return 0;
    }else{
        return 1;
    }
}

/*Returnera antalet noder i listan*/
int numberOfNodesInList(const List list)
{
    if(isEmpty(list)){
        return 0;
    }

    int nodeCount = 1;
    List tmpNode = list;

    while(tmpNode->next != NULL){
        tmpNode = tmpNode->next;
        nodeCount++;
    }

    return nodeCount; //Ersatt med ratt returvarde
}

/*Ta bort alla noder ur listan
  Glom inte att frigora minnet
  Postcondition: Listan ar tom, *list r NULL (testa med assert)*/
void clearList(List *list)
{
    //Alla noder maste tas avallokeras en och en, det racker inte att endast frigora list.
    List nodeToRemove = *list;

    while (!isEmpty(nodeToRemove)) {
        removeFirst(&nodeToRemove);
    }

    free(*list);
    *list = NULL;

    assert(isEmpty(*list) && *list == NULL);
}

/*Skriv ut listan
  Vid anropet kan man ange stdout som argument 2 for att skriva ut p skarmen.
  Anvanda fprintf for att skriva ut.
  Den har typen av utskriftfunktion blir mer generell da man kan valja att skriva ut till skarmen eller till fil.*/
void printList(const List list, FILE *textfile)
{
    if(isEmpty(list)){
        fprintf(textfile, "List is empty.\n");
    }else{

        List nodeToPrint = list;

        fprintf(textfile, "%d, ", nodeToPrint->data);

        while (nodeToPrint->next != NULL) {
            nodeToPrint = nodeToPrint->next;
            fprintf(textfile, "%d, ", nodeToPrint->data);
        }
    }
}

/*Returnera forsta datat i listan
  Precondition: listan ar inte tom (testa med assert)*/
Data getFirstElement(const List list)
{
    assert(!isEmpty(list));

    if(!isEmpty(list)){
        return list->data;
    }else{
        printf("List is empty.\n");
        return 0;
    }
}

/*Returnera sista datat i listan
  Precondition: listan ar inte tom (testa med assert)*/
Data getLastElement(const List list)
{
    assert(!isEmpty(list));

    if(!isEmpty(list)){
        List tmpNode = list;
        while (tmpNode->next != NULL) {
            tmpNode = tmpNode->next;
        }

        return tmpNode->data;
    }else{
        printf("List is empty.\n");
        return 0;
    }
}

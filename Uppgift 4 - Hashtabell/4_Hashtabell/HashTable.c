#define _CRT_SECURE_NO_WARNINGS // Behovs for vissa funktioner i visual studio
#include "HashTable.h"
#include "Bucket.h"
#include<assert.h>
#include<stdlib.h>
#include<stdio.h>

    // Anvands for att markera en ledig plats i Hashtabellen


/* Denna funktion tar en nyckel och returnerar ett hash-index
dvs ett index till arrayen som r Hashtabellen */
static int hash(Key key, int tablesize)
{
    return key % tablesize;
}

/*Leta framt enligt principen ppen adressering
 Antalet krockar returneras via pekaren col i parameterlistan*/
static int linearProbe(const HashTable* htable, Key key, unsigned int *col)
{
    if(htable){
        int tableIndex = hash(key, htable->size);
        int startIndex = tableIndex;

        //Går igenom alla index tills ett tomt hittas
        while(htable->table[tableIndex].key != key && htable->table[tableIndex].key != UNUSED){
            *col += 1;
            tableIndex++;

            if(tableIndex >= htable->size){
                tableIndex = 0;
            }

            if(tableIndex == startIndex){
                //Returnerar -1 om inga lediga platser finns
                return -1;
            }
        }

        return tableIndex;
    }

    //Returnera -1 om htable inte finns, alltså finns inga lediga platser
    return -1;
}

//Arrangerar om ett index om det finns en bättre plats för den (post deleteElement)
static void optimizeTableIndex(HashTable* htable, Key key, int tableIndex)
{
    int startIndex = tableIndex;
    unsigned int collisions = 0;

    //Kör igenom alla index en gång
    do{
        startIndex++;

        if(startIndex >= htable->size){
            startIndex = 0;
        }

        //Om en nyckel finns och inte ligger optimalt, ta bort den och lägg till den på optimal plats som vanligt
        if(htable->table[startIndex].key != UNUSED && linearProbe(htable, htable->table[startIndex].key, &collisions) != startIndex){
            Key tmpKey = htable->table[startIndex].key;
            Value tmpValue = htable->table[startIndex].value;

            htable->table[startIndex].key = UNUSED;
            insertElement(htable, tmpKey, tmpValue);
        }
    }while(startIndex != tableIndex);
}

/*Allokera minne fr hashtabellen*/
HashTable createHashTable(unsigned int size)
{
    struct Bucket* table = (struct Bucket*) malloc(sizeof(struct Bucket) * size);
    if(!table){
        printf("ERROR: Failed to allocate memory for \"table\" in file hasTable.c, function createHashTable.\n");
        exit(EXIT_FAILURE);
    }

    //Alla keys ska vara UNUSED innan de har används (duh...)
    for(int i = 0; i < size; i++){
        table[i].key = UNUSED;
    }

    HashTable htable;

    htable.table = table;
    htable.size = size;

    return htable;
}

/* Satter in paret {key,data} i Hashtabellen, om en nyckel redan finns ska vardet uppdateras */
/* Returnerar antalet krockar (som rknas i linearProbe() )*/
unsigned int insertElement(HashTable* htable, const Key key, const Value value)
{
    if(htable){
        unsigned int collisions = 0;
        int tableIndex = linearProbe(htable, key, &collisions);

        if(tableIndex == -1){
            return collisions;
        }

        if(htable->table[tableIndex].key == key){
            htable->table[tableIndex].value = value;
        }
        else{
            htable->table[tableIndex].key = key;
            htable->table[tableIndex].value = value;
        }

        return collisions;
    }

    // Postcondition: det finns ett element for key i tabellen (anvand lookup() for att verifiera)
    assert(lookup(htable, key));
    return 0; //Ersatt med ratt varde
}

/* Tar bort datat med nyckel "key" */
void deleteElement(HashTable* htable, const Key key)
{
    if(htable){
        unsigned int collisions = 0;
        int tableIndex = linearProbe(htable, key, &collisions);

        if(htable->table[tableIndex].key == key && htable->table[tableIndex].key != UNUSED){
            htable->table[tableIndex].key = UNUSED;
        }

        optimizeTableIndex(htable, key, tableIndex);
    }
    // Postcondition: inget element med key finns i tabellen (anvand loookup() for att verifiera)
    assert(lookup(htable, key) == NULL);
}

/* Returnerar en pekare till vardet som key ar associerat med eller NULL om ingen sadan nyckel finns */
const Value* lookup(const HashTable* htable, const Key key)
{
    unsigned int collisions = 0;
    int tableIndex = linearProbe(htable, key, &collisions);

    //Om linearProbe gick bra och om det finns en person på den platsen
    if(tableIndex != -1 && htable->table[tableIndex].key == key){
        return &(htable->table[tableIndex].value);
    }

    //Annars
    return NULL;
}

/* Tommer Hashtabellen */
void freeHashTable(HashTable* htable)
{
    free(htable->table);
    htable->table = NULL;
    htable->size = 0;
    // Postcondition: hashtabellen har storlek 0
}

/* Ger storleken av Hashtabellen */
unsigned int getSize(const HashTable* htable)
{
    return htable->size;
}

/* Denna for att ni enkelt ska kunna visualisera en Hashtabell */
void printHashTable(const HashTable* htable)
{
    for(int i = 0; i < htable->size; i++){
        if(htable->table[i].key == UNUSED){
            printf("%d --- UNUSED\n", i);
        }
        else{
            printPerson(&htable->table[i].value, i);
            printf("\n");
        }
    }
    // Tips: anvand printPerson() i Person.h for att skriva ut en person
}

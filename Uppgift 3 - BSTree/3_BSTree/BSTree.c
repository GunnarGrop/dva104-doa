#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <math.h>
#include "BSTree.h"

/*Det r helt tilltet att lgga till egna hjlpfunktioner men de befintliga funktionerna fr inte ndras*/

/* Statiska hjalpfunktioner anvands av andra funktioner i tradet och ska inte ligga i interfacet (anvandaren behover inte kanna till dessa) */


/* Skapar en tradnod med det givna datat genom att allokera minne for noden. Glom inte att initiera pekarna*/
static struct treeNode* createNode(int data)
{
    // Glom inte att testa sa att allokeringen lyckades
    BSTree newTreeNode = (BSTree) calloc(sizeof (BSTree), 1);
    if(!newTreeNode){
        fprintf(stderr, "ERROR: Failed to allocate memory in createNode.\n");
        //Om det inte går att allokera minne finns det ingenting att göra i varje fall
        exit(EXIT_FAILURE);
    }

    newTreeNode->data = data;
    //newTreeNode-> left & right är redan NULL, pga calloc

    return newTreeNode; // Ersatt med ratt returvarde
}

//Går igenom hela trädet (in order) och skriver all data sorterat till en array
static void traverseTree(const BSTree tree, int *sortedArray, int *index)
{
    if(!isEmpty(tree)){
        if(!tree->left && !tree->right){
            sortedArray[*index] = tree->data;
            *index = *index + 1;
        }
        else{
            if(tree->left){
                traverseTree(tree->left, sortedArray, index);
            }

            sortedArray[*index] = tree->data;
            *index = *index + 1;

            if(tree->right){
                traverseTree(tree->right, sortedArray, index);
            }
        }
    }
}

/* Returnerar en dynamiskt allokerad array som innehaller tradets data sorterat */
static int* writeSortedToArray(const BSTree tree)
{
    /* Skapa en dynamisk array men ratt storlek

       Skriv datat frn tradet sorterat till arrayen (minsta till storsta)
       - till detta kanske du behover en hjalpfunktion */

    int nodeAmount = numberOfNodes(tree);
    int *sortedArray = (int*) malloc(sizeof(int) * nodeAmount);

    if(!sortedArray){
        printf("ERROR: Failed to allocate memory in writeSortedArray.\n");
        exit(EXIT_FAILURE);
    }

    int index = 0;
    traverseTree(tree, sortedArray, &index);

    return sortedArray; //Ersatt med korrekt returvarde
}

/* Bygger upp ett sorterat, balanserat trad fran en sorterad array */
static void buildTreeSortedFromArray(BSTree* tree, const int array[], int arraySize)
{
    /* Bygg rekursivt fran mitten.
       Mittenelementet i en delarray skapar rot i deltradet
       Vanster delarray bygger vanster deltrad
       Hoger delarray bygger hoger deltrad*/

    //Bode alltid avrundas likadant om ojämt antal
    int middleIndex = arraySize / 2;

    if(arraySize > 0){
        BSTree newNode = createNode(array[middleIndex]);

        //Bygger vänster delträd först
        buildTreeSortedFromArray(&(newNode->left), array, middleIndex);
        /*Bygger sedan höger delträd
        Eftersom arr är en int-pekare kan vi stega igenom den med ints
        size - middleIndex - 1 är viktigt på grund av hur ints avrundas, middleIndex - 1 fungerar ej*/
        buildTreeSortedFromArray(&(newNode->right), array + middleIndex + 1, arraySize - middleIndex - 1);
        *tree = newNode;
    }
}


/* Implementation av tradet, funktionerna i interfacet */

/* Skapar ett tomt trad - denna funktion ar fardig */
BSTree emptyTree(void)
{
    return NULL;
}

/* Returnerar 1 ifall tradet ar tomt, 0 annars */
int isEmpty(const BSTree tree)
{
    if(tree){
        return 0;
    }
    return 1;
}

/* Satter in 'data' sorterat i *tree
 Post-condition: data finns i tradet*/
void insertSorted(BSTree* tree, int data)
{
    /*Tank pa att tradet kan vara tomt vid insattning
      Du bestammer sjalv hur dubletter ska hanteras, ska de inte accepteras eller
      ska de laggas till vanster/hoger?.
      Post-condition kan verifieras med hjalp av find(...)*/

    //*tree är inte alltid rotnoden, eftersom senare skickas left & right in i funktionen
    if(isEmpty(*tree)){
        BSTree newNode = createNode(data);
        //Programmet kraschar om det inte går att allokera minne (se createNode)
        *tree = newNode;
    }
    else{
        /*Kom ihåg: left & right behöver ej bestämmas eftersom de adresserna
        redan skickas in till funktionen, där en ny nod skapas och länkas*/
        if(data < (*tree)->data){
            insertSorted(&(*tree)->left, data);
        }
        else if(data > (*tree)->data){
            insertSorted(&(*tree)->right, data);
        }
    }

    //Gör ingenting om data redan finns i trädet
}

/* Utskriftsfunktioner
   Vid anrop: anvand stdio som andra argument for att skriva ut p skarmen
   Det racker att ni implementerar LR ordningarna*/

//Dessa kan optimeras genom att inte kalla på isEmpty. if(tree) räcker.
void printPreorder(const BSTree tree, FILE *textfile)
{
    if(!isEmpty(tree)){
        fprintf(textfile, "%d ", tree->data);
        printPreorder(tree->left, textfile);
        printPreorder(tree->right, textfile);
    }
}

void printInorder(const BSTree tree, FILE *textfile)
{
    if(!isEmpty(tree)){
        printInorder(tree->left, textfile);
        fprintf(textfile, "%d ", tree->data);
        printInorder(tree->right, textfile);
    }
}

void printPostorder(const BSTree tree, FILE *textfile)
{
    if(!isEmpty(tree)){
        printPostorder(tree->left, textfile);
        printPostorder(tree->right, textfile);
        fprintf(textfile, "%d ", tree->data);
    }
}

/* Returnerar 1 om 'data' finns i tree, 0 annars */
int find(const BSTree tree, int data)
{
    // Tank pa att tradet kan vara tomt
    if(isEmpty(tree)){
        return 0;
    }

    //Går igenom trädet fram tills data hittas
    if(data == tree->data){
        return 1;
    }
    else if(data < tree->data && tree->left){
        return find(tree->left, data);
    }
    else if(data > tree->data && tree->right){
        return find(tree->right, data);
    }

    return 0;
}

/*Returnerar adressen till en nod. Tack till Quadragoon på GitHub.
Fungerar på samma sätt som find, bara att den returnerar en treeNode-pekare*/
BSTree* findNodeAdress(BSTree *tree, int data)
{
    if(isEmpty(*tree)){
        return NULL;
    }

    //Går igenom trädet fram tills adressen hittas
    if(data == (*tree)->data){
        return tree;
    }
    else if(data < (*tree)->data && (*tree)->left){
        return findNodeAdress(&(*tree)->left, data);
    }
    else if(data > (*tree)->data && (*tree)->right){
        return findNodeAdress(&(*tree)->right, data);
    }

    return NULL;
}

//Returnerar antalet barn för en nod
int numberOfChildren(const BSTree tree)
{
    //Om både left & right är NULL har noden inga barn
    if(!tree->left && !tree->right){
        return 0;
    }
    //Om både left & right finns har noden två barn
    else if(tree->left && tree->right){
        return 2;
    }
    //Annars har noden ett barn
    else{
        return 1;
    }
}

/* Tar bort 'data' fran tradet om det finns */
void removeElement(BSTree* tree, int data)
{
    /* Inget data ska/kan tas bort fran ett tomt trad
     Tre fall: Ett lov (inga barn), ett barn (vanster eller hoger), tva barn

     Glom inte att frigora noden nar den lankats ur tradet*/

    if(isEmpty(*tree)){
        return;
    }

    BSTree *nodeToRemove = findNodeAdress(tree, data);
    if(!nodeToRemove){
        return;
    }
    int amountOfChildren = numberOfChildren(*nodeToRemove);

    //Om noden är ett löv
    if(amountOfChildren == 0){
        free(*nodeToRemove);
        *nodeToRemove = NULL;
    }
    //Om noden har 1 barn
    else if(amountOfChildren == 1){
        BSTree tmpNode = *nodeToRemove;

        if(tmpNode->right){
            *nodeToRemove = tmpNode->right;
            free(tmpNode);
        }
        else if(tmpNode->left){
            *nodeToRemove = tmpNode->left;
            free(tmpNode);
        }
    }
    //Om noden har två barn
    else if(amountOfChildren == 2){
        BSTree tmpNode = *nodeToRemove;

        tmpNode = tmpNode->right;
        while(tmpNode->left){
            tmpNode = tmpNode->left;
        }
        int tmpData = tmpNode->data;

        removeElement(tree, tmpData);
        (*nodeToRemove)->data = tmpData;
    }

    assert(!find(*tree, data));
}

/* Returnerar hur manga noder som totalt finns i tradet */
int numberOfNodes(const BSTree tree)
{
    if(isEmpty(tree)){
        return 0;
    }

    //Kommer gå igenom all noder och plussa på 1 för varje nod
    return numberOfNodes(tree->left) + numberOfNodes(tree->right) + 1;
}

/* Returnerar hur djupt tradet ar */
int depth(const BSTree tree)
{
    if(isEmpty(tree)){
        return 0;
    }

    int leftBranch = depth(tree->left);
    int rightBranch = depth(tree->right);

    //Kommer returna den 'väg' som är längst till sista lövet
    if(leftBranch > rightBranch){
        return leftBranch + 1;
    }
    else{
        return rightBranch + 1;
    }
}

/* Returnerar minimidjupet for tradet
   Se math.h for anvandbara funktioner*/
int minDepth(const BSTree tree)
{
    if(isEmpty(tree)){
        return 0;
    }

    /*Log2 av antalet noder + 1, avrunda uppåt.
     Borde korrekt typkonvertera sig själv?*/
    return ceil(log2(numberOfNodes(tree) + 1));
}

/* Balansera tradet sa att depth(tree) == minDepth(tree) */
void balanceTree(BSTree* tree)
{
    /* Forslag pa algoritm:
       - overfor tradet till en dynamiskt allokerad array med writeSortedToArray()
       - tom tradet med freeTree()
       - bygg upp tradet rekursivt fran arrayen med buildTreeSortedFromArray()
       - frigor minne for den dynamiskt allokerade arrayen


       Post-conditions:
       - tree har lika manga noder som tidigare
       - djupet for tradet ar samma som minimumdjupet for tradet */

    int nodeAmount = numberOfNodes(*tree);
    int *sortedArray = writeSortedToArray(*tree);

    freeTree(tree);
    buildTreeSortedFromArray(tree, sortedArray, nodeAmount);
    free(sortedArray);
}

/* Tom tradet och frigor minnet for de olika noderna */
void freeTree(BSTree* tree)
{
    if(!isEmpty(*tree)){
        freeTree(&(*tree)->left);
        freeTree(&(*tree)->right);
        free(*tree);
        *tree = NULL;
    }

    assert(isEmpty(*tree));
    // Post-condition: tradet ar tomt
}




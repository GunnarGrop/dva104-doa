#include "Stack.h"
#define NDEBUG
#include<assert.h>

/*Funktionen ar fardig*/
Stack initializeStack(void)
{
    return createEmptyList();
}

int stackIsEmpty(const Stack stack)
{
    return isEmpty(stack);
}

/* Postcondition 'data' ligger overst p stacken */
void push(Stack* stack, const Data data)
{
    addLast(stack, data);

    assert(getLastElement(*stack) == data);
}

/* Precondition: stacken far inte vara tom */
void pop(Stack* stack)
{
    assert(!isEmpty(*stack));

    if(!isEmpty(*stack)){
        removeLast(stack);
    }else{
        printf("Stack is empty.\n");
    }
}

/* Precondition: stacken far inte vara tom */
Data peekStack(const Stack stack)
{
    assert(!isEmpty(stack));

    if(!isEmpty(stack)){
        return getLastElement(stack);
    }else{
        printf("Stack is empty.\n");
    }
}

/* Anvands for testning och felsokning
 Tips: det kan vara bra att ocksa notera i urskriften vart toppen pa stacken ar */
void printStack(const Stack stack, FILE *textFile)
{
    printList(stack, textFile);
}

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    SortingAlgorithms.c \
    Statistics.c \
    main.c \
    Array.c

HEADERS += \
    SortingAlgorithms.h \
    Statistics.h \
    Array.h
